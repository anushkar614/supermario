package game;

import edu.monash.fit2099.engine.positions.Ground;
import game.actors.Player;
import game.grounds.WarpPipe;
import game.items.Bottle;

import java.util.ArrayList;

/**
 * A class to check if an Actor or Item is an instance of a specific class and to return the object of that type
 * without using instanceof or downcasting.
 */
public class InstanceChecker {

    /**
     * ArrayList containing all Players.
     */
    private ArrayList<Player> playerList;

    private ArrayList<Bottle> bottleList;

    private ArrayList<WarpPipe> warpPipeList;

    /**
     * The class representing the instance of the StatusManager.
     */
    private static InstanceChecker instance;

    /**
     * Constructor of the Instance Checker class.
     */
    public InstanceChecker(){
        playerList = new ArrayList<>();
        bottleList = new ArrayList<>();
        warpPipeList = new ArrayList<>();
    }

    /**
     * Gets the instance of the StatusManager, creates one if it doesn't exist.
     * @return the instance of the StatusManager
     */
    public static InstanceChecker getInstance(){
        if(instance == null){
            instance = new InstanceChecker();
        }
        return instance;
    }

    /**
     * Adds a {@link Player} to the playerList.
     * @param player the Player to be added
     */
    public void addToPlayer(Player player){
        this.playerList.add(player);
    }

    /**
     * Gets the entire list of Players.
     * @return the ArrayList of Players
     */
    public ArrayList<Player> getPlayers(){
        return new ArrayList<Player>(this.playerList);
    }

    /**
     * Getting the warp pipes
     * @return the warp pipe list
     */
    public ArrayList<WarpPipe> getWarpPipes() {
        return warpPipeList;
    }

    /**
     * Adding the warp pipe
     * @param wp
     */
    public void addWarpPipe(WarpPipe wp) {
        this.warpPipeList.add(wp);
    }

    /**
     * Gets the single Warp pipe
     * @param ground
     * @return wp if wp is ground and returns null
     */
    public WarpPipe getSingleWarpPipe(Ground ground){
        for(WarpPipe wp : warpPipeList){
            if(wp == ground){
                return wp;
            }
        }
        return null;
    }
}
