package game;

import java.util.Arrays;
import java.util.List;

import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.displays.Display;
import edu.monash.fit2099.engine.positions.FancyGroundFactory;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.positions.Location;
import edu.monash.fit2099.engine.positions.World;

import game.actors.*;
import game.grounds.*;
import game.items.Coin;
import game.items.SuperMushroom;

/**
 * The main class for the Mario World game.
 *
 */
public class Application {

	/**
	 * The main function
	 * @param args
	 */
	public static void main(String[] args) {

			World world = new World(new Display());

			FancyGroundFactory groundFactory = new FancyGroundFactory(new Dirt(), new Wall(), new Floor(), new TreeSprout(), new HealthFountain(), new PowerFountain(), new WarpPipe());

			List<String> map = Arrays.asList(
				"....C.....................................##..........+.........................",
				"..C.........+........C...+..................#..............C................C...",
				"............................................#...................................",
				".................................C...........##...............C......+..........",
				"......................C.........C..............#................................",
				"................................................#...............................",
				".................+................................#.............................",
				".................C...............................##................C....C.......",
				"...................................CC............##.............................",
				".........+..............................+#____####.................+............",
				".....C..........................H...A..+#_____###++.............................",
				".......................................+#______###.................C............",
				"...................CC....................+#_____###.............................",
				"......C.................+.......C................##.............+...............",
				"................................................C..#.................CC.........",
				"......C.............................................#...........................",
				"...................+.................................#..........................",
				"........C.....................C.......................#.........C...............",
				".......................C.....................CC........##.......................");

			GameMap gameMap = new GameMap(groundFactory, map);
			world.addGameMap(gameMap);
			
			FancyGroundFactory groundFactory2 = new FancyGroundFactory(new Dirt(), new TreeSprout(), new PowerFountain(), new HealthFountain(), new Lava(), new WarpPipe());

		    List<String> map2 = Arrays.asList(
					"C.......L..............L....A.....................+...................",
					".......................................................H........+.....",
					"............+...........AAA..........................A................",
					"............H.............................L...........................",
					"..................................................+......A............",
					"....+.................HH..LL.......L..........................+.......",
					"............................................++........................",
					"...LL....H........................+.........................LL........",
					"...................................A..................................",
					"....................+........LL.LL...+..................L..+..........",
					"..................A...................................................",
					"..............+.......................................................",
					"................L........................A.......H....................");

			GameMap gameMap2 = new GameMap(groundFactory2, map2);
			world.addGameMap(gameMap2);

			// sets all the map1 warp pipes to teleport to the single map2 pipe
		    for(int y = 0; y < map.size(); y++){
				String row = map.get(y);
				for(int x = 0; x < row.length(); x++){
					if(row.charAt(x) == 'C'){
						WarpPipe wp = InstanceChecker.getInstance().getSingleWarpPipe(gameMap.at(x,y).getGround());
						wp.setDestinationPipe(gameMap2.at(0,0));
						wp.setDestMapName("Lava Map");
					}
				}
			}
			// sets map2 warp pipe name (Ensures 0,0 on fire map is always a warp pipe.)
			gameMap2.at(0, 0).setGround(new WarpPipe());
		    InstanceChecker.getInstance().getSingleWarpPipe(gameMap2.at(0,0).getGround()).setDestMapName("Grass");

			Actor mario = new Player("Player", 'm', 100);
			world.addPlayer(mario, gameMap.at(42, 10));

			gameMap.at(44, 10).addActor(new Toad());
			gameMap.at(42, 10).addItem(new SuperMushroom());
			gameMap.addActor(new Toad(), gameMap.at(40, 9));
			gameMap.addActor(new FlyingKoopa(), gameMap.at(29, 11));
			gameMap.addActor(new Bowser(), gameMap.at(35, 4));
			gameMap2.addActor(new PrincessPeach(), gameMap2.at(5, 1));

			world.run();

	}
}
