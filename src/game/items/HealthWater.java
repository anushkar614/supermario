package game.items;

import edu.monash.fit2099.engine.actors.Actor;

/**
 * Health Water that is supplied by Health Fountains
 * Drinking this water will increase the drinker's hit points by 50
 */
public class HealthWater extends Water {

    private static final int BONUS_ATTACKDAMAGE = 0;
    private static final int BONUS_HITPOINTS = 50;

    /**
     * Constructor for the Health Water class
     */
    public HealthWater() {
        super("Health Water", 'H', false);
    }

    /**
     * Heals the drinker's hit points by 50 after consuming Health Water
     * @param actor the actor that consumes the Health Water
     * @return an integer representing the number of bonus IntrinsicAttackDamage points
     */
    public int consume(Actor actor) {
        // Increase drinker's hit points/healing by 50 hit points.
        actor.heal(BONUS_HITPOINTS);
        return BONUS_ATTACKDAMAGE;
    }

}
