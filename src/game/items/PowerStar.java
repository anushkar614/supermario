package game.items;

import edu.monash.fit2099.engine.items.Item;

import game.behaviour.PowerStarAction;

/**
 * A power star class which makes mario invulnerable, able to turn high ground
 * into coins and one shot enemies.
 */
public class PowerStar extends Item {
    /**
     * Constructor passes to parent constructor default values.
     */
    public PowerStar() {
        super("Power Star", '*', true);
        addAction(new PowerStarAction());
    }
}
