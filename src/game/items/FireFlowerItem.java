package game.items;

import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.items.Item;

import game.behaviour.FireFlowerAction;

/**
 * A public class fire flower which makes mario's attack spread fire, the fire
 * stays on the ground for 3 turns. The effect lasts 20 turns.
 */
public class FireFlowerItem extends Item {
    /**
     * Constructor passes to parent constructor default values.
     */
    public FireFlowerItem() {
        super("Power Star", 'f', true);
        addAction(new FireFlowerAction());
    }
}
