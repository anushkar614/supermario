package game.items;

import edu.monash.fit2099.engine.items.Item;

import game.behaviour.SuperMushroomAction;

/**
 * A super mushroom which allows for mario to jump without chance of failure
 * until damaged, also increases mario's max hp by 50 permanently.
 */
public class SuperMushroom extends Item {
    /**
     * Constructor passes to parent constructor default values.
     */
    public SuperMushroom() {
        super("Super Mushroom", '^', true);
        addAction(new SuperMushroomAction());
    }
}
