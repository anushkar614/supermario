package game.items;

import edu.monash.fit2099.engine.weapons.WeaponItem;
import edu.monash.fit2099.engine.items.DropItemAction;
import edu.monash.fit2099.engine.actors.Actor;

import game.behaviour.Status;

/**
 * A wrench weapon item which can be used to break koopa shells.
 * @see game.behaviour.Status#BREAKER
 */
public class WrenchItem extends WeaponItem {
	 /**
     * Constructor passes to parent constructor default values.
     */
	public WrenchItem() {
		// Name, symbol, damage, verb, hitchance/100.
		super("Wrench", '.', 50, "swings a wrench at", 80);
		addCapability(Status.BREAKER);
	}

	/**
	 * Prevents the item from being dropped.
	 * @param actor the actor who won't be able to drop the item.
	 */
	@Override
	public DropItemAction getDropAction(Actor actor) {
		// Item should not be able to be dropped.
		return null;
	}
}