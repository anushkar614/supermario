package game.items;

import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.items.Item;

/**
 * A base water class holds functionality used by all water subclasses
 * Water that is supplied by fountains
 */
public abstract class Water extends Item {

    /**
     * Creates a new water item
     * @param name the name of the water
     * @param displayChar the display character of the water
     * @param portable indicates if water is portable
     */
    public Water(String name, char displayChar, boolean portable) {
        super(name, displayChar, false);
    }

    /**
     * An abstract method to initiate effects after consuming water item
     * @param actor
     * @return
     */
    public abstract int consume(Actor actor);
}
