package game.items;

import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.items.Item;
import edu.monash.fit2099.engine.positions.Location;
import game.actors.BottleComponent;
import game.actors.ComponentActor;
import game.actors.ComponentId;
import game.behaviour.DrinkWaterAction;

import static game.behaviour.Status.HAS_BOTTLE;

/**
 * A public class represents Bottle extending item which allows actors to obtain water from fountains
 */
public class Bottle extends Item {

    /**
     * Creates a new bottle item
     */
    public Bottle() {
        super("Bottle", 'b', false);
        this.addCapability(HAS_BOTTLE);
    }

    /**
     * Provides the drink water action to actors if the bottle is not empty
     * @param actor the Actor acting
     * @param location the current Location
     * @param direction the direction of the Ground from the Actor
     * @see ComponentId#BOTTLE_COMPONENT
     */
    public ActionList allowableActions(Actor actor, Location location, String direction) {
        BottleComponent bottle = (BottleComponent)((ComponentActor)actor).getComponent(ComponentId.BOTTLE_COMPONENT);
        if (!bottle.isEmpty()) {
            return new ActionList(new DrinkWaterAction());
        }
        return new ActionList();
    }
}
