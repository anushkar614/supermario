package game.items;

import edu.monash.fit2099.engine.items.Item;
import edu.monash.fit2099.engine.positions.Location;

/**
 * Public class represents Fire
 */
public class Fire extends Item {

    private int counter;

    /***
     * Constructor of the class Fire
     */
    public Fire() {
        super("Fire", 'v', false);
        this.counter = 0;
    }

    /**
     * Whenever bowser will attack, the fire will be dropping on the ground that will last for 3 turns which the counter
     * will check.
     * @param currentLocation The location of the ground on which we lie.
     */
    @Override
    public void tick(Location currentLocation){
        if(counter == 3){
            currentLocation.removeItem(this);
        }
        else if(currentLocation.containsAnActor()){
            currentLocation.getActor().hurt(20);
        }
        counter++;
    }
}