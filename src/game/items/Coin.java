package game.items;

import edu.monash.fit2099.engine.items.Item;
import edu.monash.fit2099.engine.items.PickUpItemAction;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.positions.Location;

import game.behaviour.AddToWalletAction;
import game.behaviour.Resettable;
import game.ResetManager;

/**
 * Coin is spawned from sapling trees or destroyed high grounds.
 */
public class Coin extends Item implements Resettable{
	private int value;
	private int locationX;
	private int locationY;

	/**
	 * Creates a new coin item.
	 * @param value the value of the coin item in a wallet.
	 * @param location the location of the coin object for resetting it.
	 */
	public Coin(int value, Location location) {
		super("Coin", '$', true);
		// Register instance for resetting.
		locationX = location.x();
		locationY = location.y();
		registerInstance();
		this.value = value;
	}

	/**
	 * Allows for the item to be picked up.
	 * @param actor
	 * @return
	 */
	@Override
	public PickUpItemAction getPickUpAction(Actor actor) {
		return new AddToWalletAction(value, this);
	}

	/**
	 * Deletes coins on reset.
	 * @param map the map the coin exists on.
	 */
	@Override
    public void resetInstance(GameMap map) {
        map.at(locationX, locationY).removeItem(this);
    }
}