package game.items;

import edu.monash.fit2099.engine.actors.Actor;

/**
 * Power Water that is supplied by Power Fountains
 * Drinking this water will increase the drinker's intrinsic attack damage by 15
 */
public class PowerWater extends Water {

    private static final int BONUS_ATTACKDAMAGE = 15;

    /**
     * Constructor of the PowerWater class
     */
    public PowerWater() {
        super("Power Water", 'A', false);
    }

    /**
     * Returns an integer indicating the number of bonus IntrinsicAttackDamage points for the drinker
     * @param actor the actor that consumes the Power Water
     * @return an integer representing the number of bonus IntrinsicAttackDamage points
     */
    public int consume(Actor actor) {
        // Increase drinker's base/intrinsic attack damage by 15
        return BONUS_ATTACKDAMAGE;
    }
}
