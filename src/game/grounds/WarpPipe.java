package game.grounds;

import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.positions.Ground;
import edu.monash.fit2099.engine.positions.Location;
import game.InstanceChecker;
import game.behaviour.Status;
import game.behaviour.TeleportAction;
import game.actors.PiranhaPlant;
import game.behaviour.Resettable;

/**
 * Public class representing Warp pipe
 */
public class WarpPipe extends Ground implements Resettable {

    private Location destinationPipe;

    private String destMapName;

    private boolean hasSpawned;

    /***
     * Constructor of the class Warp Pipe
     */
    public WarpPipe() {
        super('C');
        this.destinationPipe = null;
        InstanceChecker.getInstance().addWarpPipe(this);
        this.destMapName = null;
        this.hasSpawned = false;
        this.registerInstance();
    }

    /**
     * The method tick for checking if the actor is standing on the location and taking action accordingly.
     * @param location The location of the Ground
     * @see Status#TO_RESET
     * @see Status#BLOCKED
     */
    @Override
    public void tick(Location location) {
        if(!location.containsAnActor()) {
            if(this.hasCapability(Status.TO_RESET)) {
                this.removeCapability(Status.TO_RESET);
                if(!location.containsAnActor()) {
                    location.addActor(new PiranhaPlant());
                }
                this.addCapability(Status.BLOCKED);
            }
            else if(!this.hasSpawned){
                location.addActor(new PiranhaPlant());
                this.addCapability(Status.BLOCKED);
                this.hasSpawned = true;
            }
        }
    }

    /**
     * Public function ActionList checks for the allowable Actions
     * @param actor the Actor acting
     * @param location the current Location
     * @param direction the direction of the Ground from the Actor
     * @return actions
     * @see Status#BLOCKED
     */
    @Override
    public ActionList allowableActions(Actor actor, Location location, String direction) {
        ActionList actions = new ActionList();
        if(location.containsAnActor()) {
            if (location.getActor().getDisplayChar() != 'Y' && !this.hasCapability(Status.BLOCKED)) {
                actions.add(new TeleportAction(location, destinationPipe, destMapName));
            }
        } else{
            this.removeCapability(Status.BLOCKED);
        }

        return actions;
    }

    /**
     * Setting the destination of the pipe
     * @param destinationPipe
     */
    public void setDestinationPipe(Location destinationPipe) {
        this.destinationPipe = destinationPipe;
    }

    /**
     * Public function setDestMapName which sets the destination of the map name to the destination
     * @param destMapName
     */
    public void setDestMapName(String destMapName) {
        this.destMapName = destMapName;
    }

    /**
     * Public function which checks if the actor can enter or not
     * @param actor the Actor to check
     * @return a boolean value about if the actor's status is blocked
     */
    @Override
    public boolean canActorEnter(Actor actor){
        return !this.hasCapability(Status.BLOCKED);
    }

    /**
     * Allows any classes that use this interface to reset abilities, attributes, and/or items.
     * @param map
     */
    @Override
    public void resetInstance(GameMap map) {
        this.addCapability(Status.TO_RESET);
    }

    /**
     * Public function registerInstance which is generating a new the register Instance
     */
    @Override
    public void registerInstance() {
        Resettable.super.registerInstance();
    }
}
