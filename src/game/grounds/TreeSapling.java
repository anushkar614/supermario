package game.grounds;

import edu.monash.fit2099.engine.positions.Location;

import game.items.Coin;

/**
 * The seconds stage of development for a Tree, spawns coins.
 */
public class TreeSapling extends Tree {
	static final int GROWTH_AGE = 10;
	static final double SPAWN_CHANCE = 0.1;

	static final double JUMP_SUCCESS = 0.8;
	static final int FALL_DAMAGE = 20;

	public TreeSapling() {
		super('t', JUMP_SUCCESS, FALL_DAMAGE);
	}

	/**
	 * Attempt to spawn coins on its location and age into a Mature Tree.
	 * @param location the location of this tree
	 */
	@Override
	protected void grow(Location location) {
		if (Math.random() < SPAWN_CHANCE) {
			location.addItem(new Coin(20, location));
		}
		if (getAge() == GROWTH_AGE) {
			location.setGround(new TreeMature());
		}
	}
}