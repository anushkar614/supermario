package game.grounds;

import game.items.HealthWater;

/**
 * A public class representing Health Fountain which supplies Health Waters to other actors
 */
public class HealthFountain extends Fountain {

    public HealthFountain() {
        super('H');
        water = new HealthWater();
    }

}
