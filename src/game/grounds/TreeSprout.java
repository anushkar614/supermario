package game.grounds;

import edu.monash.fit2099.engine.positions.Location;

import game.actors.Goomba;

/**
 * The first stage of development for a Tree, spawns goombas.
 */
public class TreeSprout extends Tree {
	static final int GROWTH_AGE = 10;
	static final double SPAWN_CHANCE = 0.1;

	static final double JUMP_SUCCESS = 0.9;
	static final int FALL_DAMAGE = 10;

	public TreeSprout() {
		super('+', JUMP_SUCCESS, FALL_DAMAGE);
	}

	/**
	 * Attempt to spawn a Goomba and age into a Tree Sapling
	 * @param location the location of this tree
	 */
	@Override
	protected void grow(Location location) {
		if (!location.containsAnActor() && Math.random() < SPAWN_CHANCE) {
			location.addActor(new Goomba());
		}
		if (getAge() == GROWTH_AGE) {
			location.setGround(new TreeSapling());
		}
	}
}