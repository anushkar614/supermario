package game.grounds;

import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.Ground;
import edu.monash.fit2099.engine.positions.Location;
import game.InstanceChecker;

/**
 * Class representing the new map Lava zone
 */
public class Lava extends Ground {
    /**
     * Constructor
     */
    public Lava() { super('L'); }

    /**
     * Checks if the Actor can enter
     * @param actor the Actor to check
     * @return boolean if the actor can enter or not
     */
    @Override
    public boolean canActorEnter(Actor actor) {
        return InstanceChecker.getInstance().getPlayers().contains(actor);
    }

    /**
     * Inflicts 15 damage per turn when the player steps on it
     * Enemies cannot step on this
     * @param location The location of the Ground
     */
    @Override
    public void tick(Location location) {
        try {
            location.getActor().hurt(15);
        } catch(Exception e){
        }
    }
}
