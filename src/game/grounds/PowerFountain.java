package game.grounds;

import game.items.PowerWater;

/**
 * A public class Power Fountain which supplies Power Waters to other actors
 */
public class PowerFountain extends Fountain {

    /**
     * Constructor of PowerFountain
     */
    public PowerFountain() {
        super('A');
        water = new PowerWater();
    }

}
