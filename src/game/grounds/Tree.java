package game.grounds;

import edu.monash.fit2099.engine.positions.Ground;
import edu.monash.fit2099.engine.positions.Location;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actors.Actor;

import game.behaviour.JumpAction;
import game.behaviour.Status;
import game.behaviour.Resettable;
import game.grounds.Dirt;
import game.ResetManager;
import game.items.FireFlowerItem;

/**
 * The base tree class holds functionality used by all tree.
 * Jumping, Age, Resetting, etc.
 */
public abstract class Tree extends Ground implements Resettable {
    // Chance a fire flower is spawned at this tree's location.
    private static final double FIRE_FLOWER_CHANCE = 0.5;

    private int age;
    private int locationX;
    private int locationY;
    private double jumpSuccess;
    private int fallDamage;

    /**
     * Constructor for all trees.
     * @param character the character the tree will be represented by.
     * @param jumpSuccess the success chance of jumping onto the tree.
     * @param fallDamage the damage taken from failing to jump onto the tree.
     * @see game.behaviour.Status#JUMPABLE
     */
    protected Tree(char character, double jumpSuccess, int fallDamage) {
        super(character);
        // Register instance for resetting.
        registerInstance();
        this.jumpSuccess = jumpSuccess;
        this.fallDamage = fallDamage;
        addCapability(Status.JUMPABLE);
    }

    protected int getAge() {
        return age;
    }

    /**
     * Used for resetting mature tree age every growth attempt.
     */
    protected void resetAge() {
        age = 0;
    }

    /**
     * Tells nearby jumping actors they must jump on this ground to reach it.
     * Provides the jumping action.
     * @see game.behaviour.Status#CAN_JUMP
     */
    @Override
    public ActionList allowableActions(Actor actor, Location location, String direction) {
        locationX = location.x();
        locationY = location.y();
        if (actor.hasCapability(Status.CAN_JUMP) && direction != "") {
            return new ActionList(new JumpAction(jumpSuccess, fallDamage, location, direction));
        }
        return new ActionList();
    }

    /**
     * Grows the tree and increases its age. Also has a 50% chance to spawn
     * a fire flower item on its location each turn.
     * @param location The location of the tree on its GameMap
     */
    @Override
    public void tick(Location location) {
        grow(location);
        if (Math.random() < FIRE_FLOWER_CHANCE) {
            location.addItem(new FireFlowerItem());
        }
        age++;
    }

    /**
     * When a reset occurs all trees have a 50% chance of becoming dirt.
     * @param map Gamemap the reset is occurring on.
     */
    @Override
    public void resetInstance(GameMap map){
        if (Math.random() < 0.5) {
            map.at(locationX, locationY).setGround(new Dirt());
        }
    }

    /**
     * Makes trees impassible.
     */
    @Override
    public boolean canActorEnter(Actor actor) {
        if (actor.hasCapability(Status.POWERED)){
            return true;
        }
        return false;
    }

    /**
     * Used for tree type functionality.
     * Override to add additional functionality to a tree.
     */
    protected abstract void grow(Location location);
}
