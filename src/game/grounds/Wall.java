package game.grounds;

import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.Ground;
import edu.monash.fit2099.engine.positions.Location;
import edu.monash.fit2099.engine.actions.ActionList;

import game.behaviour.JumpAction;
import game.behaviour.Status;

/**
 * Impassible ground that can be jumped over.
 */
public class Wall extends Ground {

	static final double JUMP_SUCCESS = 0.8;
	static final int FALL_DAMAGE = 20;

	/**
	 * Public constructor for the wall class
	 * @see game.behaviour.Status#JUMPABLE
	 */
	public Wall() {
		super('#');
		addCapability(Status.JUMPABLE);
	}

	/**
    * Tells nearby jumping actors they must jump on this ground to reach it.
    * Provides the jumping action.
    * @see game.behaviour.Status#CAN_JUMP
    */
	@Override
    public ActionList allowableActions(Actor actor, Location location, String direction) {
        if (actor.hasCapability(Status.CAN_JUMP) && direction != "") {
            return new ActionList(new JumpAction(JUMP_SUCCESS, FALL_DAMAGE, location, direction));
        }
        return new ActionList();
    }

	/**
	 * Checks if the actor can enter
	 * @param actor the Actor to check
	 * @return boolean of the same
	 */
	@Override
	public boolean canActorEnter(Actor actor) {
		if (actor.hasCapability(Status.POWERED)){
			return true;
		}
		return false;
	}

	/**
	 * Blocks thrown objects over this ground.
	 * @return true
	 */
	@Override
	public boolean blocksThrownObjects() {
		return true;
	}
}
