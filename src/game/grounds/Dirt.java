package game.grounds;

import edu.monash.fit2099.engine.positions.Ground;

import game.behaviour.Status;

/**
 * A class that represents bare dirt.
 * @see game.behaviour.Status#FERTILE
 */
public class Dirt extends Ground {

	/**
	 * Public constructor of the class Dirt
	 */
	public Dirt() {
		super('.');
		addCapability(Status.FERTILE);
	}
}
