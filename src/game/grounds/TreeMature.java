package game.grounds;

import java.util.ArrayList;

import edu.monash.fit2099.engine.positions.Location;

import game.actors.Koopa;
import game.behaviour.Status;

/**
 * A mature tree which will attempt to spawn new sprouts, spawn koopas, and
 * has a chance to decay with each turn.
 */
public class TreeMature extends Tree {
	static final int GROWTH_AGE = 5;
	static final double SPAWN_CHANCE = 0.15;
	static final double DECAY_CHANCE = 0.2;

	static final double JUMP_SUCCESS = 0.7;
	static final int FALL_DAMAGE = 30;

	public TreeMature() {
		super('T', JUMP_SUCCESS, FALL_DAMAGE);
	}

	/**
	 * Checks surrounding ground for fertile land and randomly places a
	 * sprout there. Also randomly spawns Koopas and has a low chance to decay.
	 * @param location the location of this tree
	 * @see game.behaviour.Status#FERTILE
	 */
	@Override
	protected void grow(Location location) {
		if (!location.containsAnActor() && Math.random() < SPAWN_CHANCE) {
			location.addActor(new Koopa("Flying Koopa", 'F', 150));
		}
		// Try to spawn sprout in surrounding fertile square.
		if (getAge() == GROWTH_AGE) {
			ArrayList<Integer[]> options = new ArrayList<Integer[]>();
			for(int xOff = -1; xOff <= 1; xOff++) {
				for(int yOff = -1; yOff <= 1; yOff++) {
					int locationX = location.x() + xOff;
					int locationY = location.y() + yOff;
					// Skip out of bounds locations.
					if (locationX < 0 || locationX > location.map().getXRange().max() 
						|| locationY < 0 || locationY > location.map().getYRange().max()) {
						continue;
					}
					Location target = location.map().at(locationX, locationY);
					if (target.getGround().hasCapability(Status.FERTILE)) {
						options.add(new Integer[]{xOff, yOff});
					}
				}
			}
			if (options.size() == 0) {
				return;
			}
			Integer[] randOff = options.get((int)Math.floor(Math.random() * options.size()));
			location.map().at(location.x() + randOff[0], location.y() + randOff[1]).setGround(new TreeSprout());
			resetAge();
		}
		// Attempt to decay
		if (Math.random() < DECAY_CHANCE) {
			location.setGround(new Dirt());
		}
	}
}