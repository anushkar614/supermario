package game.grounds;

import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.Ground;
import edu.monash.fit2099.engine.positions.Location;
import game.behaviour.FillBottleAction;
import game.behaviour.Status;
import game.items.Water;

import static game.behaviour.Status.SUPPLY_WATER;

/**
 * The base fountain class holds functionality used by all fountains
 * Supplies infinite amount of waters to other actors
 */
public abstract class Fountain extends Ground {

    protected Water water;

    /**
     * Constructor for all fountains
     * @param character the character the fountain will be represented by
     * @see game.behaviour.Status#SUPPLY_WATER
     */
    protected Fountain(char character) {
        super(character);
        this.addCapability(SUPPLY_WATER); // Fountains can supply Water items to Actors
    }

    /**
     * Provides the fill bottle action to actors who are on top of fountain and possess
     * a bottle in their inventory
     * @param actor the Actor acting
     * @param location the current Location
     * @param direction the direction of the Ground from the Actor
     * @see game.behaviour.Status#HAS_BOTTLE
     * @see game.behaviour.Status#ON_FOUNTAIN
     */
    @Override
    public ActionList allowableActions(Actor actor, Location location, String direction) {

        // Check if Actor has a Bottle item
        // and if Actor has Status.ON_FOUNTAIN which indicates Actor is standing on this Fountain
        // If both conditions met, Actor is able to perform FillBottleAction
        if(actor.hasCapability(Status.HAS_BOTTLE) && actor.hasCapability(Status.ON_FOUNTAIN)) {

            // Remove Status.ON_FOUNTAIN as Actor may move away from Fountain Ground this turn
            actor.removeCapability(Status.ON_FOUNTAIN);
            return new ActionList(new FillBottleAction(water));
        }

        return new ActionList();
    }
}
