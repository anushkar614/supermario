package game.grounds;

import edu.monash.fit2099.engine.positions.Ground;
import edu.monash.fit2099.engine.actors.Actor;

import game.behaviour.Status;

/**
 * A class that represents the floor inside a building.
 * @see game.behaviour.Status#ALLERGIC_TO_FLOOR
 */
public class Floor extends Ground {
	public Floor() {
		super('_');
	}

	/**
	 * Prevents actors who shouldn't walk on floors from doing so.
	 */
	@Override
	public boolean canActorEnter(Actor actor) {
		if (actor.hasCapability(Status.ALLERGIC_TO_FLOOR)){
			return false;
		}
		return true;
	}
}
