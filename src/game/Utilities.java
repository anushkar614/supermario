package game;

/**
 * Public class which represents Utilities
 */
public class Utilities {
    private int turn = 0;
    private static Utilities instance;

    /**
     * The get instance method for Utilities
     * @return the utilities instance
     */
    public static Utilities getInstance() {
        if(instance == null){
            instance = new Utilities();
        }
        return instance;
    }

    /**
     * Constructor of Utilities
     */
    private Utilities() {}

    /**
     * To keep track of the turn count
     */
    public void tick() {
    	turn += 1;
    }

    /**
     * To get the turn of the next
     */
    public int getTurn() {
    	return turn;
    }
}