package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.positions.Location;
import game.actors.TimerComponent;
import game.grounds.Dirt;
import game.items.Coin;

import game.behaviour.Status;
import game.actors.ComponentActor;

/**
 * A public class which represents the power star action
 */
public class PowerStarAction extends Action {

    /**
     * The execute function for the Power Star Action
     * @param actor The actor performing the action.
     * @param map The map the actor is on.
     * @return invokes the menu description function
     */
    @Override
    public String execute(Actor actor, GameMap map) {
        ((ComponentActor)actor).addComponent(new TimerComponent(Status.POWERED, 10));
        actor.addCapability(Status.POWERED);
        actor.heal(200);
        return menuDescription(actor);
    }

    /**
     * Throws a message on the menu description for the user
     * @param actor The actor performing the action.
     * @return the message to be displayed
     */
    public String menuDescription(Actor actor) {
        return actor + " has consumed Power Star";
    }

}
