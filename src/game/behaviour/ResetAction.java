package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.actors.Actor;

import game.ResetManager;

/**
 * Used by the player to perform the reset action once per game.
 * @see game.ResetManager
 * @see game.behaviour.Resettable
 */
public class ResetAction extends Action {

	/**
	 * Performs the reset by calling the resetInstance() method of all
	 * resettables registered.
	 * @see game.behaviour.Resettable#registerInstance()
	 */
	@Override
	public String execute(Actor actor, GameMap map) {
		ResetManager.getInstance().run(map);
		return actor + " reset the game.";
	}

	/**
	 * Lets the player know the game was reset successfully
	 * @param actor The actor performing the action.
	 * @return the message in the menu description
	 */
	@Override
	public String menuDescription(Actor actor) {
		return "Reset the game.";
	}
}