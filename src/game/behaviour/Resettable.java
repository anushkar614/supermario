package game.behaviour;

import edu.monash.fit2099.engine.positions.GameMap;

import game.ResetManager;


/**
 * Public interface resettable which helps in getting the game to the reset mode.
 */
public interface Resettable {

    /**
     * Allows any classes that use this interface to reset abilities, attributes, and/or items.
     * @param map
     */
    void resetInstance(GameMap map);

    /**
     * A default interface method that register current instance to the Singleton manager.
     * It allows corresponding class uses to be affected by global reset
     */
    default void registerInstance() {
        ResetManager.getInstance().appendResetInstance(this);
    }
}
