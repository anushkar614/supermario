package game.behaviour;

import java.util.Random;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.items.Item;
import edu.monash.fit2099.engine.weapons.Weapon;

import static game.behaviour.Status.TALL;
import game.items.Fire;

/**
 * Special Action for attacking other Actors.
 */
public class AttackAction extends Action {

	/**
	 * The Actor that is to be attacked
	 */
	protected Actor target;

	/**
	 * The direction of incoming attack.
	 */
	protected String direction;

	/**
	 * Random number generator
	 */
	protected Random rand = new Random();

	/**
	 * Constructor of Attack Action
	 * 
	 * @param target the Actor to attack
	 * @param direction the Direction Attacking
	 */
	public AttackAction(Actor target, String direction) {
		this.target = target;
		this.direction = direction;
	}

	/**
	 * Constructor of Attack Action
	 * 
	 * @param target the Actor to attack
	 */
	public AttackAction(Actor target) {
		this.target = target;
	}

	/**
	 * Executes the attack action
	 * @param actor The actor performing the action.
	 * @param map The map the actor is on.
	 * @return the result of the execution
	 */
	@Override
	public String execute(Actor actor, GameMap map) {

		Weapon weapon = actor.getWeapon();

		if (!(rand.nextInt(100) <= weapon.chanceToHit()) && !target.hasCapability(Status.POWERED)) {
			return actor + " misses " + target + ".";
		}

		int damage = weapon.damage();
		String result = actor + " " + weapon.verb() + " " + target + " for " + damage + " damage.";
		target.hurt(damage);

		// If player was attacked, remove Super Mushroom effect
		if (actor.hasCapability(Status.TALL)) {
			actor.removeCapability(Status.TALL);
		}

		// Don't kill actor if it can become dormant.
		if (!target.isConscious()) {
			ActionList dropActions = new ActionList();
			// drop all items
			for (Item item : target.getInventory())
				dropActions.add(item.getDropAction(actor));
			for (Action drop : dropActions)
				drop.execute(target, map);
			// If actor can become dormant dont kill it.
			if (target.hasCapability(Status.BECOME_DORMANT)) {
				target.addCapability(Status.DORMANT);
				result += System.lineSeparator() + target + " retreats into its shell.";
			} 
			else {
				map.removeActor(target);
				result += System.lineSeparator() + target + " is killed.";
			}
		}
		// Spawn fire at target's location if actor has the FIRE_ATTACK status.
		if (actor.hasCapability(Status.FIRE_FLOWER_ATTACK)) {
			map.locationOf(target).addItem(new Fire());
		}

		return result;
	}

	/**
	 * For displaying message to the User
	 * @param actor The actor performing the action.
	 * @return the message to be displayed to the user
	 */
	@Override
	public String menuDescription(Actor actor) {
		if (direction == null) {
			return actor + " attacks " + target;
		}
		String attackString = actor + " attacks " + target + " at " + direction;
		if (actor.hasCapability(Status.FIRE_FLOWER_ATTACK)) {
			attackString = attackString + "with fire!";
		}
		return attackString;
	}

}
