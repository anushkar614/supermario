package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.positions.Location;
import game.InstanceChecker;
import game.grounds.WarpPipe;

/**
 * Public class Teleport Action for Mario to be able to shift to the Map 2.
 */
public class TeleportAction extends Action {

    private Location source;
    private Location destination;
    private String mapName;

    /**
     * Public constructor of the teleport action class
     * @param src
     * @param dest
     * @param name
     */
    public TeleportAction(Location src, Location dest, String name){
        this.source = src;
        this.destination = dest;
        this.mapName = name;
    }

    /**
     * Function to execute the Teleport Action
     * @param actor The actor performing the action.
     * @param map The map the actor is on.
     * @return the menu description
     */
    @Override
    public String execute(Actor actor, GameMap map) {
        WarpPipe destWarpPipe = InstanceChecker.getInstance().getSingleWarpPipe(destination.getGround());
        destWarpPipe.setDestinationPipe(source);
        if(destination.containsAnActor()){
            map.removeActor(destination.getActor());
        }
        map.moveActor(actor, destination);
        return actor + "has teleported to the " + mapName;
    }

    /**
     * Function menuDescription to write the description
     * @param actor The actor performing the action.
     * @return the string
     */
    @Override
    public String menuDescription(Actor actor) {
        return actor + " teleports to the " + mapName;
    }
}
