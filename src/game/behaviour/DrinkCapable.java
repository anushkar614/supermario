package game.behaviour;


/**
 * Interface that can be implemented by actors to indicate
 * they can drink water from fountains
 */
public interface DrinkCapable {

    /**
     * Adds bonus points to intrinsic attack damage from consuming water from Power Fountain
     * @param bonus Additional intrinsic damage points to be added to the accummulated bonus
     */
    public void addBonusAttackDamage(int bonus);

}
