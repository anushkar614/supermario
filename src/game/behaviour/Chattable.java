package game.behaviour;

import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.displays.Display;

import game.Utilities;

/**
 * Public interface chattable to display all the chat messages.
 */
public interface Chattable {
	/**
	 * Causes a actor to chat.
	 * @param actor The actor chatting.
	 * @param display The display to chat in.
	 * @param messages The possible messages to chat.
	 */
	default public void chat(Actor actor, Display display, String[] messages) {
		if (Utilities.getInstance().getTurn() % 2 == 0) {
			display.println(actor + ": " + messages[(int) (Math.random() * messages.length)]);
		}
	}
}