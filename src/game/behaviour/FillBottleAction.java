package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;
import game.actors.ComponentActor;
import game.actors.ComponentId;
import game.actors.BottleComponent;
import game.items.Water;

/**
 * A class to represent an action to fill water into the bottle
 */
public class FillBottleAction extends Action {
    private final Water water;


    /**
     * Constructor for the FillBottleAction
     * @param water the water item to be filled into the bottle
     */
    public FillBottleAction(Water water) {
        this.water = water;
    }

    /**
     * Actor fills a water item into the bottle
     * @param actor The actor performing the action.
     * @param map The map the actor is on.
     * @return a message indicating the actor as filled a water item into the bottle
     */
    public String execute(Actor actor, GameMap map) {
        BottleComponent bottle = (BottleComponent)((ComponentActor)actor).getComponent(ComponentId.BOTTLE_COMPONENT);
        bottle.fillBottle(water);
        return actor + " refilled " + water;
    }

    /**
     * Describes the type of water to be filled into the bottle
     * @param actor The actor performing the action.
     * @return the text to be displayed on the menu
     */
    public String menuDescription(Actor actor) {
        return actor + " refill " + water;
    }

}
