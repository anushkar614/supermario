package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;

/**
 * Public class to kill an actor if they wish to destroy themselves.
 */
public class SuicideAction extends Action {

	/**
	 * The function to execute the Suicide action
	 * @param actor the actor to kill.
	 * @param map the map where the actor to kill exists.
	 */
	@Override
	public String execute(Actor actor, GameMap map) {
		map.removeActor(actor);
		return actor + " committed suicide.";
	}

	/**
	 * The function to print in the menu description
	 * @param actor The actor performing the action.
	 * @return the message
	 */
	@Override
	public String menuDescription(Actor actor) {
		return "Commit Suicide";
	}
}