package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;

import game.items.Fire;

/**
 * The class to represent the Fire Attack Action.
 */
public class FireAttackAction extends Action {

    private Actor target;

    private String direction;

    /**
     * The contructor of the class Fire Attack Action.
     * @param target
     * @param direction
     */
    public FireAttackAction(Actor target, String direction){
        this.target = target;
        this.direction = direction;
    }

    /**
     * The public function execute to execute the action of the fire attacking.
     * @param actor The actor performing the action.
     * @param map The map the actor is on.
     * @return
     */
    @Override
    public String execute(Actor actor, GameMap map) {
        map.locationOf(target).addItem(new Fire());
        return actor + " dropped fire at " + target + "'s location.";
    }

    /**
     * Throws the message to the user.
     * @param actor The actor performing the action.
     * @return the message
     */
    @Override
    public String menuDescription(Actor actor) {
        return actor + " attacks " + target + " at " + direction + " with fire!";
    }
}