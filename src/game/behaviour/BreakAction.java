package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.actors.Actor;

import game.items.SuperMushroom;

/**
 * Breaks the target breakable actor dropping a Super Mushroom.
 */
public class BreakAction extends Action {
	private Actor target;
	private String direction;

	/**
	 * Action constructor sets target and target direction.
	 * @param target the target breakable actor.
	 * @param direction the direction of the target breakable actor.
	 */
	public BreakAction(Actor target, String direction) {
		this.target = target;
		this.direction = direction;
	}

	/**
	 * Breaks the target spawning a super mushroom.
	 * @param actor the actor breaking the target.
	 * @param map the map where the target and actor reside.
	 */
	@Override
	public String execute(Actor actor, GameMap map) {
		map.locationOf(target).addItem(new SuperMushroom());
		map.removeActor(target);
		return actor + " breaks the shell of the koopa revealing a Super Mushroom!";
	}

	/**
	 * Displays the target and direction as an option for breaking.
	 * @param actor The actor performing the action.
	 * @return the message for the user
	 */
	@Override
	public String menuDescription(Actor actor) {
		return actor + " breaks " + target + " at " + direction;
	}
}