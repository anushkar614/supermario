package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.items.Item;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.actors.Actor;

import game.actors.WalletComponent;
import game.actors.ComponentId;
import game.actors.ComponentActor;

/**
 * Purchase a given item using a given cost from a wallet if the actor
 * is has a wallet and adds it to their inventory.
 * @see game.actors.Toad
 */
public class PurchaseItemAction extends Action {
	private Item item;
	private int value;

	/**
	 * @param item the item being sold.
	 * @param value the price of the item being sold.
	 */
	public PurchaseItemAction(Item item, int value) {
		this.item = item;
		this.value = value;
	}

	/**
	 * Sells the given item to the given actor if they have the required funds
	 * and a wallet.
	 * @param actor the actor attempting to buy the item.
	 * @param map the map where the purchase is occurring.
	 */
	@Override
	public String execute(Actor actor, GameMap map) {
		if (actor.hasCapability(Status.HAS_WALLET)){
			WalletComponent wallet = (WalletComponent)((ComponentActor)actor).getComponent(ComponentId.WALLET_COMPONENT);
			if (wallet.getWalletValue() >= value) {
				wallet.removeFromWallet(value);
				actor.addItemToInventory(item);
				return actor + " purchased the " + item + " for $" + value + ". New balance: $" + wallet.getWalletValue();
			}
			else {
				return "You don't have enough coins!";
			}
		}
		return actor + " doesn't have a wallet.";
	}

	/**
	 * Lists the item as a sale and the price of it as a menu item.
	 */
	@Override
	public String menuDescription(Actor actor) {
		return actor + " buys " + item + " ($" + value + ")";
	}
}