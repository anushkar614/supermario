package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;

import static game.behaviour.Status.TALL;

/**
 * Public class which represents Super Mushroom
 */
public class SuperMushroomAction extends Action {

    /**
     * The function which executes Action
     * @param actor The actor performing the action.
     * @param map The map the actor is on.
     * @return invokes the menu description
     */
    @Override
    public String execute(Actor actor, GameMap map) {
        actor.addCapability(TALL);
        actor.increaseMaxHp(50);
        return menuDescription(actor);
    }

    /**
     * Prints the desired string
     * @param actor The actor performing the action.
     * @return the string to be printed
     */
    public String menuDescription(Actor actor) {
        return actor + " has consumed Super Mushroom";
    }

}
