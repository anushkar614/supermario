package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;
import game.actors.ComponentActor;
import game.actors.ComponentId;
import game.actors.BottleComponent;
import game.actors.Player;
import game.items.Water;

/**
 * A class to represent an action to drink water from the bottle
 */
public class DrinkWaterAction extends Action {

    /**
     * Actor consumes a water item from the bottle
     * @param actor The actor performing the action.
     * @param map The map the actor is on.
     * @return a message indicating the actor as consumed water from the bottle
     * @see ComponentId#BOTTLE_COMPONENT
     */
    public String execute(Actor actor, GameMap map) {

        BottleComponent bottle = (BottleComponent)((ComponentActor)actor).getComponent(ComponentId.BOTTLE_COMPONENT);

        // Get Water from Bottle
        Water water = bottle.drinkFromBottle();
        // Then let Actor consume Water
        // Bonus Intrinsic Attack Damage points returned if Power Water was consumed
        int bonusAttackDamage = water.consume(actor);

        // Add to Intrinsic Attack Damage bonus
        ((DrinkCapable)actor).addBonusAttackDamage(bonusAttackDamage);

        return actor + " consumes " + water;
    }

    /**
     * Returns a descriptive string indicating the collection of water items in the bottle
     * @param actor The actor performing the action.
     * @return the text to be displayed on the menu
     * @see ComponentId#BOTTLE_COMPONENT
     */
    public String menuDescription(Actor actor) {
        BottleComponent bottle = (BottleComponent)((ComponentActor)actor).getComponent(ComponentId.BOTTLE_COMPONENT);
        return actor + " consume Bottle" + (bottle.getBottleValues()).toString(); // to update
    }
}