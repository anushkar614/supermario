package game.behaviour;

/**
 * Use this enum class to give `buff` or `debuff`.
 * It is also useful to give a `state` to abilities or actions that can be attached-detached.
 */
public enum Status {
    HOSTILE_TO_ENEMY, // Use this status to be considered hostile towards enemy (e.g., to be attacked by enemy)
    TALL, // Use this status to tell that current instance has "grown".
    FERTILE, // Used to specify if new sprouts may grow on this ground.
    CAN_JUMP, // use this status to jump from lower to higher ground
    POWERED, // use this status to indicate consumption of Power Star by actor
    BECOME_DORMANT, // Does the actor have the ability to avoid death by becoming dormant.
    DORMANT, // The actor is currently dormant and hiding.
    ALLERGIC_TO_FLOOR, // The actor cannot enter floor grounds.
    HAS_WALLET, // Allows for the collection of coins and trading.
    BREAKER, // Has the ability to break particularly hard actors.
    JUMPABLE, //Can jump
    TO_RESET, //Can reset itself
    CAN_RESET, //Can reset
    FIRE_ATTACK, //Can do fire attack
    BLOCKED, //indicates block by the enemy
    FIGHTING, //Indicates can fight
    FLYING, // Indicates this ground can be jumped over.
    KEY, // For interaction with Princess Peach to end the game after defeating Bowser
    HAS_BOTTLE, // Allows actor obtain water from fountain to fill up bottle
    ON_FOUNTAIN, // Use this status to indicate Actor is standing on a Fountain
    SUPPLY_WATER, // Use this status to indicate Ground can supply Water items to other Actors
    FIRE_FLOWER_ATTACK, // Attacks will spread fire at the target's location.
}
