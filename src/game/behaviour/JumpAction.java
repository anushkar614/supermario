package game.behaviour;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actions.MoveActorAction;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.Exit;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.positions.Ground;
import edu.monash.fit2099.engine.positions.Location;
import game.grounds.Dirt;
import game.items.Coin;

/**
 * Action for jumping to high ground.
 */
public class JumpAction extends Action {

    private final double jumpSuccess;
    private final int fallDamage;
    private final Location destination;
    private boolean jumpedLastTurn;

    /**
     * The direction to jump
     */
    protected String direction;

    /**
     * Initializes the jump with a set of characteristics.
     * @param jumpSuccess the chance the jump succeeds as a percentile.
     * @param fallDamage the damage taken if the jump fails.
     * @param location the location jumping to.
     * @param direction the direction of the jump used for the menu.
     */
    public JumpAction(double jumpSuccess, int fallDamage, Location location, String direction) {
        this.jumpSuccess = jumpSuccess;
        this.fallDamage = fallDamage;
        this.destination = location;
        this.direction = direction;
        this.jumpedLastTurn = true;
    }

    /**
     * Executes the jump based on the initialized characteristics.
     * @param actor the actor performing the jump.
     * @param map the map where the jump is occurring.
     */
    @Override
    public String execute(Actor actor, GameMap map) {
        if (Math.random() < jumpSuccess || actor.hasCapability(Status.TALL)) {
            map.moveActor(actor, destination);
            return actor + " has succeeded the jump and has moved " + direction + ".";
        } else {
            actor.hurt(fallDamage);
            return actor + " has failed a jump and has received " + fallDamage + " damage.";
        }
    }

    /**
     * Describes the jump and the direction it will go in as a menu item.
     */
    public String menuDescription(Actor actor) {
        return actor + " will jump " + direction + ".";
    }
}
