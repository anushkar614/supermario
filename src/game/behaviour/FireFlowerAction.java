package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.positions.Location;
import game.actors.TimerComponent;
import game.grounds.Dirt;
import game.items.Coin;

import game.behaviour.Status;
import game.actors.ComponentActor;

/**
 * Public class to represent the Fire Flower Action which causes the actor's attacks to spread fire.
 * @see game.items.Fire
 */
public class FireFlowerAction extends Action {

    /**
     * Public function execute to execute the Action of actor's attakcs being spread as fire.
     * @param actor The actor executing the action.
     * @param map The gamemap where the action occurs.
     * @return the message
     */
    @Override
    public String execute(Actor actor, GameMap map) {
        ((ComponentActor)actor).addComponent(new TimerComponent(Status.FIRE_FLOWER_ATTACK, 20));
        actor.addCapability(Status.FIRE_FLOWER_ATTACK);
        return actor + " consumed the Fire Flower.";
    }

    /**
     * The public menu description for throwing the description to the user
     * @param actor The actor performing the action.
     * @return the message
     */
    public String menuDescription(Actor actor) {
        return actor + " will consume the Fire Flower.";
    }

}
