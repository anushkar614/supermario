package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.items.PickUpItemAction;
import edu.monash.fit2099.engine.items.Item;

import game.actors.ComponentActor;
import game.actors.WalletComponent;
import game.actors.ComponentId;

/**
 * Adds a specified amount of value to a wallet.
 * Used by the Coin item.
 * @see game.items.Coin#getPickUpAction(Actor actor)
 */
public class AddToWalletAction extends PickUpItemAction {
	private int value;
	private Item item;

	/**
	 * Contructor set the value and item to pickup.
	 * @param value the value of the item being picked up.
	 * @param item the item to place into wallet (destroy).
	 */
	public AddToWalletAction(int value, Item item) {
		super(null);
		this.value = value;
		this.item = item;
	}

	/**
	 * Places the value inside an actor's wallet if the actor has one, and
	 * delete the item if it was placed in the wallet.
	 * @param actor the actor who's wallet to place the value in.
	 * @param map the map where the actor and item exist.
	 * @see Status#HAS_WALLET
	 */
	@Override
	public String execute(Actor actor, GameMap map) {
		if (actor.hasCapability(Status.HAS_WALLET)){
			WalletComponent wallet = (WalletComponent)((ComponentActor)actor).getComponent(ComponentId.WALLET_COMPONENT);
			wallet.addToWallet(value);
			map.locationOf(actor).removeItem(item);
			return actor + " added $" + value + " to their wallet. New balance: $" + wallet.getWalletValue();
		}
		return actor + " doesn't have a wallet.";
	}

	/**
	 * Displays the value of the coin to pickup as a menu item.
	 * @param actor The actor performing the action.
	 * @return the description to be displayed on the menu
	 */
	@Override
	public String menuDescription(Actor actor) {
		return "Pickup coin of value: $" + value;
	}
}