package game.behaviour;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.items.Item;
import edu.monash.fit2099.engine.positions.GameMap;

/**
 * A class to represent an action to kill off an Actor
 *
 */
public class DeathAction extends Action {

    /**
     * Kills off the Actor performing the action
     *
     * @param actor The actor performing the action.
     * @param map   The map the actor is on.
     * @return a message saying that the Actor committed suicide
     */
    public String execute(Actor actor, GameMap map) {
        String message;
        ActionList dropActions = new ActionList();
        // drop all items
        for (Item item : actor.getInventory())
            dropActions.add(item.getDropAction(actor));
        for (Action drop : dropActions)
            drop.execute(actor, map);
        message = (actor + " dies.");
        map.removeActor(actor);
        return message;
    }

    /**
     * Returns a descriptive string
     *
     * @param actor The actor performing the action.
     * @return the text we put on the menu
     */
    @Override
    public String menuDescription(Actor actor) {
        return (actor.toString() + "does not want to live anymore.");
    }
}