package game.actors;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actions.DoNothingAction;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.displays.Display;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.positions.Location;
import edu.monash.fit2099.engine.weapons.IntrinsicWeapon;
import game.InstanceChecker;
import game.behaviour.Status;
import game.behaviour.DeathAction;
import game.behaviour.FireAttackAction;
import game.behaviour.AttackAction;
import game.behaviour.Behaviour;
import game.behaviour.FollowBehaviour;
import game.items.Key;
import game.behaviour.Resettable;
import game.behaviour.Chattable;

import java.util.HashMap;
import java.util.Map;

/**
 * Class to represent Bowser
 */
public class Bowser extends Actor implements Resettable, Chattable {
    // Potential Chat Messages
    static final String[] MESSAGES = {"What was that sound? Oh, just a fire.",
        "Princess Peach! You are formally invited... to the creation of my new kingdom!",
        "Never gonna let you down!", 
        "Wrrrrrrrrrrrrrrrryyyyyyyyyyyyyy!!!!"};


    private final Map<Integer, Behaviour> behaviours = new HashMap<>(); // priority, behaviour
    private boolean firstRound;             // counter
    private Location originalLocation;
    private boolean inCombat = false;

    /**
     * Constructor of the public class Bowser
     */
    public Bowser() {
        super("Bowser", 'B', 500);
        this.firstRound = true;
        this.originalLocation = null;
        this.addItemToInventory(new Key()); //for dropping the key to unlock Princess Peach's handcuff
        this.registerInstance();
        this.addCapability(Status.FIGHTING);
    }

    /**
     * Figures out what to do accordingly.
     * @param actions    collection of possible Actions for this Actor
     * @param lastAction The Action this Actor took last turn. Can do interesting things in conjunction with Action.getNextAction()
     * @param map        the map containing the Actor
     * @param display    the I/O object to which messages may be written
     * @return the appropriate action.
     */
    @Override
    public Action playTurn(ActionList actions, Action lastAction, GameMap map, Display display) {
        // Perform Chat
        chat(this, display, MESSAGES);
        if(!this.isConscious()){
            return new DeathAction();
        }

        if (this.hasCapability(Status.TO_RESET)) {             // moving it to the original location if reset is ongoing
            this.removeCapability(Status.TO_RESET);
            try {
                map.moveActor(this, this.originalLocation);
            } catch (Exception ignored){}
            this.heal(this.getMaxHp());                       // getting back to max
            this.behaviours.remove(2);
            return new DoNothingAction();
        }

        if (firstRound){
            this.originalLocation = map.locationOf(this);           // saving the original location
            this.firstRound = false;
        }

        // This definitely wasn't the solution you were looking for but works
        // pretty well and doesn't break the engine in any way. Couldn't
        // figure out how to use the action list for this.
        Location location = map.locationOf(this);
        for(int xOff = -1; xOff <= 1; xOff++) {
            for(int yOff = -1; yOff <= 1; yOff++) {
                int locationX = location.x() + xOff;
                int locationY = location.y() + yOff;
                // Skip out of bounds locations.
                if (locationX < 0 || locationX > location.map().getXRange().max()
                        || locationY < 0 || locationY > location.map().getYRange().max()) {
                    continue;
                }
                Actor target = map.getActorAt(location.map().at(locationX, locationY));
                if (target != null) {
                    if (target.hasCapability(Status.HOSTILE_TO_ENEMY)) {
                        if (!inCombat) {
                            behaviours.put(5, new FollowBehaviour(target));
                            inCombat = true;
                        }
                        return new AttackAction(target);
                    }
                }
            }
        }

        for(Behaviour Behaviour : behaviours.values()) {                // doing the suitable behaviour and action
            Action action = Behaviour.getAction(this, map);
            if (action != null)
                return action;
        }

        // default action as the Bowser has to stand still the Mario stands next to him
        return new DoNothingAction();
    }

    /**
     * This will make the Bowser stand still, waiting for Mario on the second map. Once Mario stands next to him,
     * it will make the Bowser attack and follow Mario. Whenever Bowser attacks, a fire will be dropped on the ground.
     * It also implements the following stats: 500 hit points "punch" attack with a 50% hit rate and 80 damage.
     * Resetting the game (r command) will move Bowser back to the original position, heal it to maximum, and it will
     * stand there until Mario is within Bowser's attack range. Chat is activated.
     * @param otherActor   collection of possible Actions for this Actor
     * @param direction The Action this Actor took last turn. Can do interesting things in conjunction with Action.getNextAction()
     * @param map       the map containing the Actor
     * @return make the Bowser dead if it isn't conscious or does nothing if the reset is happening or attack the target
     * when the Mario is standing next to him or does all the suitable behaviour and action.
     */
    @Override
    public ActionList allowableActions(Actor otherActor, String direction, GameMap map) {
        if (InstanceChecker.getInstance().getPlayers().contains(otherActor)) {
            this.behaviours.put(2, new FollowBehaviour(otherActor));
        }

        ActionList actions = new ActionList();
        // it will only be attacked by the HOSTILE opponent
        if(otherActor.hasCapability(Status.HOSTILE_TO_ENEMY)) {
            actions.add(new AttackAction(this,direction));
            if(otherActor.hasCapability(Status.FIRE_ATTACK)){
                actions.add(new FireAttackAction(this, direction));
            }
        }
        return actions;
    }

    /**
     * For creating the 'kick' weapon.
     * @return weapon
     */
    @Override
    protected IntrinsicWeapon getIntrinsicWeapon(){
        return new IntrinsicWeapon(80, "punches");
    }

    /**
     * Allows any classes that use this interface to reset abilities, attributes, and/or items.
     * @param map
     */
    @Override
    public void resetInstance(GameMap map) {
        this.addCapability(Status.TO_RESET);
    }
}
