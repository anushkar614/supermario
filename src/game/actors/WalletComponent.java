package game.actors;

import edu.monash.fit2099.engine.actors.Actor;

import game.behaviour.Status;

/**
 * Support more advanced trading funtionality in the future and ensures all 
 * trading actors have the HAS_WALLET capability.
 * @see game.behaviour.Status#HAS_WALLET
 */
public class WalletComponent extends Component {
	private int wallet = 1000;

	/**
	 * Constructor class for the WalletComponent.
	 * @param actor the actor who's being given the wallet.
	 */
	public WalletComponent(Actor actor) {
		super(ComponentId.WALLET_COMPONENT);
		actor.addCapability(Status.HAS_WALLET);
	}

	/**
	 * For adding value to the wallet
	 * @param value money being added to the wallet.
	 */
	public void addToWallet(int value) {
		wallet += value;
	}

	/**
	 * For removing value from the wallet
	 * @param value money being removed from the wallet.
	 */
	public void removeFromWallet(int value) {
		wallet -= value;
	}

	/**
	 * Public function to get the value of the wallet
	 * @return wallet
	 */
	public int getWalletValue() {
		return wallet;
	}
}