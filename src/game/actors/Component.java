package game.actors;

/**
 * BaseComponent used for creating components in order to move functionality
 * away from actor classes and for safe class generalization.
 */
public abstract class Component {
	private final ComponentId componentId;

	/**
	* @param componentId used for identifying this component before generalizing.
	*/
	public Component(ComponentId componentId) {
		this.componentId = componentId;
	}

	/**
	 * To get the ComponentId
	 * @return the componentId
	 */
	public ComponentId getComponentId() {
		return componentId;
	}
}