package game.actors;

import edu.monash.fit2099.engine.actors.Actor;
import game.actors.Component;
import game.actors.ComponentId;
import game.behaviour.Status;
import game.items.Bottle;
import game.items.Water;

import java.util.Stack;

/**
 * Public class representing the Bottle Component
 * Supports advanced functionality to fill up and drink from bottle
 * Ensures actors have the HAS_BOTTLE capability
 * @see game.behaviour.Status#HAS_BOTTLE
 */
public class BottleComponent extends Component {
    Stack<Water> waterBottle = new Stack<Water>();

    /**
     * Constructor for the class BottleComponent
     * @param actor the actor who's being given the bottle
     * @see ComponentId#BOTTLE_COMPONENT
     * @see Status#HAS_BOTTLE
     */
    public BottleComponent(Actor actor) {
        super(ComponentId.BOTTLE_COMPONENT);
        actor.addItemToInventory(new Bottle());
        actor.addCapability(Status.HAS_BOTTLE);
    }

    /**
     * Checks if the bottle is empty
     * @return a boolean indicating if bottle is empty
     */
    public boolean isEmpty() {
        return waterBottle.isEmpty();
    }

    /**
     * Takes the water from the top of the stack of the bottle
     * @return the top Water item from the bottle stack
     */
    public Water drinkFromBottle() {
        return waterBottle.pop();
    }

    /**
     * Fills the bottle by placing the water on the top of the bottle stack
     * @param water the Water item to be placed on top of the bottle stack
     */
    public void fillBottle(Water water) {
        waterBottle.push(water);
    }

    /**
     * Getter to obtain the stack of waters in the bottle
     * @return the stack that stores the water items of the bottle
     */
    public Stack<Water> getBottleValues() {
        return waterBottle;
    }
}
