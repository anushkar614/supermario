package game.actors;

import java.util.HashMap;
import java.util.Map;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actions.DoNothingAction;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.displays.Display;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.weapons.IntrinsicWeapon;
import edu.monash.fit2099.engine.positions.Location;
import game.InstanceChecker;
import game.behaviour.Status;
import game.behaviour.AttackAction;
import game.behaviour.DeathAction;
import game.behaviour.Behaviour;
import game.behaviour.Resettable;

/**
 * Class representing Piranha Plant
 */
public class PiranhaPlant extends Actor implements Resettable {
    private final Map<Integer, Behaviour> behaviours = new HashMap<>(); // priority, behaviour

    /**
     * Constructor of Piranha Plant
     */
    public PiranhaPlant() {
        super("Piranha Plant", 'Y', 10);
        this.registerInstance();
        this.addCapability(Status.FIGHTING);
    }

    /**
     * Figures out what to do next
     * @param actions    collection of possible Actions for this Actor
     * @param lastAction The Action this Actor took last turn. Can do interesting things in conjunction with Action.getNextAction()
     * @param map        the map containing the Actor
     * @param display    the I/O object to which messages may be written
     * @return the action to be undertaken
     */
    @Override
    public Action playTurn(ActionList actions, Action lastAction, GameMap map, Display display) {
        // heal and add 50 hp after resetting
        if (!this.hasCapability(Status.CAN_RESET)){
            this.resetMaxHp(getMaxHp()+50);
            this.heal(getMaxHp());
        }

        if(!this.isConscious()){
            return new DeathAction();
        }
        // This definitely wasn't the solution you were looking for but works
        // pretty well and doesn't break the engine in any way. Couldn't
        // figure out how to use the actionlist for this.
        Location location = map.locationOf(this);
        for(int xOff = -1; xOff <= 1; xOff++) {
            for(int yOff = -1; yOff <= 1; yOff++) {
                int locationX = location.x() + xOff;
                int locationY = location.y() + yOff;
                // Skip out of bounds locations.
                if (locationX < 0 || locationX > location.map().getXRange().max()
                        || locationY < 0 || locationY > location.map().getYRange().max()) {
                    continue;
                }
                Actor target = map.getActorAt(location.map().at(locationX, locationY));
                if (target != null) {
                    if (target.hasCapability(Status.HOSTILE_TO_ENEMY)) {
                        return new AttackAction(target);
                    }
                }
            }
        }
        return new DoNothingAction();
    }

    /**
     * Returns collection of the Actions that the otherActor could do to the current Actor.
     * it can only be attacked by the Player.
     * @param otherActor the Actor that might perform an action.
     * @param direction  String representing the direction of the other Actor
     * @param map        current GameMap
     * @return list of actions
     * @see Status#HOSTILE_TO_ENEMY
     */
    @Override
    public ActionList allowableActions(Actor otherActor, String direction, GameMap map) {
        ActionList actions = new ActionList();
        // it can be attacked only by the HOSTILE opponent, and this action will not attack the HOSTILE enemy back.
        if(otherActor.hasCapability(Status.HOSTILE_TO_ENEMY)) {
            actions.add(new AttackAction(this,direction));
            }
        return actions;
    }

    /**
     * Adds the intrinsic weapon for Piranha Plant
     * @return new instance of the intrinsic weapon
     */
    @Override
    public IntrinsicWeapon getIntrinsicWeapon(){
        return new IntrinsicWeapon(90, "chomps");
    }

    /**
     * To allow any classes that use this interface to reset abilities, attributes, and/or items.
     * @param map
     */
    @Override
    public void resetInstance(GameMap map) {
        this.addCapability(Status.TO_RESET);
    }

}
