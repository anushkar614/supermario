package game.actors;

import java.util.ArrayList;

import edu.monash.fit2099.engine.actors.Actor;

/**
 * Abstract class representing the Componenet Actor
 * Used to allow actors to use components and store them for generalization.
 */
public abstract class ComponentActor extends Actor {
	ArrayList<Component> components = new ArrayList<Component>();

	/**
	 * The constructor of the Component Actor
	 * @param name
	 * @param displayChar
	 * @param hitPoints
	 */
	public ComponentActor(String name, char displayChar, int hitPoints) {
		super(name, displayChar, hitPoints);
	}

	/**
	 * Only one component of a type should exist in a component object.
	 * @param component the component to add.
	 */
	public void addComponent(Component component) {
		if (getComponent(component.getComponentId()) != null){
			return;
		}
		components.add(component);
	}

	/**
	 * Used to find out if a ComponentActor contains a specific component.
	 * @param componentId the component's id to search for.
	 * @return component if the component is the component Id or otherwise null.
	 */
	public Component getComponent(ComponentId componentId) {
		for (Component component : components) {
			if (componentId == component.getComponentId()) {
				return component;
			}
		}
		return null;
	}
}