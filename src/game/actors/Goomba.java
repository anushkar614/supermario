package game.actors;

import java.util.HashMap;
import java.util.Map;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.displays.Display;
import edu.monash.fit2099.engine.actions.DoNothingAction;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.weapons.IntrinsicWeapon;
import edu.monash.fit2099.engine.positions.Location;

import game.behaviour.FollowBehaviour;
import game.behaviour.AttackAction;
import game.behaviour.Behaviour;
import game.behaviour.Status;
import game.behaviour.WanderBehaviour;
import game.behaviour.SuicideAction;
import game.behaviour.Resettable;
import game.ResetManager;
import game.behaviour.Chattable;

/**
 * Public class representing Goomba
 */
public class Goomba extends ComponentActor implements Resettable, Chattable {
	private static final double SUICIDE_CHANCE = 0.1;
	// Messages used for chatting.
	private static final String[] MESSAGES = {"Mugga mugga!",
		"Ugha ugha... (Never gonna run around and desert you...)",
		"Ooga-Chaka Ooga-Ooga!"};

	private final Map<Integer, Behaviour> behaviours = new HashMap<>(); // Priority, Behaviour
	private boolean inCombat = false;

	/**
	 * Allergic to floor prevents the enemy from entering Floor ground.
	 * @see game.behaviour.Status#ALLERGIC_TO_FLOOR
	 */
	public Goomba() {
		super("Goomba", 'g', 20);
		// Register instance for resetting.
		registerInstance();
		this.behaviours.put(10, new WanderBehaviour());
		addCapability(Status.ALLERGIC_TO_FLOOR);
	}

	/**
	 /**
	 * Returns collection of the Actions that the otherActor could do to the current Actor.
	 * At the moment, we only make it can be attacked by Player.
	 * @param otherActor the Actor that might perform an action.
	 * @param direction  String representing the direction of the other Actor
	 * @param map        current GameMap
	 * @return list of actions
	 * @see Status#HOSTILE_TO_ENEMY
	 */
	@Override
	public ActionList allowableActions(Actor otherActor, String direction, GameMap map) {
		ActionList actions = new ActionList();
		// it can be attacked only by the HOSTILE opponent, and this action will not attack the HOSTILE enemy back.
		if(otherActor.hasCapability(Status.HOSTILE_TO_ENEMY)) {
			actions.add(new AttackAction(this, direction));
		}
		return actions;
	}

	/**
	 * Figure out what to do next.
	 * @see Actor#playTurn(ActionList, Action, GameMap, Display)
	 */
	@Override
	public Action playTurn(ActionList actions, Action lastAction, GameMap map, Display display) {
		// Perform Chatting
		chat(this, display, MESSAGES);
		if (Math.random() < SUICIDE_CHANCE) {
			return new SuicideAction();
		}
		// This definitely wasn't the solution you were looking for but works
		// pretty well and doesn't break the engine in any way. Couldn't
		// figure out how to use the actionlist for this.
		Location location = map.locationOf(this);
		for(int xOff = -1; xOff <= 1; xOff++) {
			for(int yOff = -1; yOff <= 1; yOff++) {
				int locationX = location.x() + xOff;
				int locationY = location.y() + yOff;
				// Skip out of bounds locations.
				if (locationX < 0 || locationX > location.map().getXRange().max() 
					|| locationY < 0 || locationY > location.map().getYRange().max()) {
					continue;
				}
				Actor target = map.getActorAt(location.map().at(locationX, locationY));
				if (target != null) {
					if (target.hasCapability(Status.HOSTILE_TO_ENEMY)) {
						if (!inCombat) {
							behaviours.put(5, new FollowBehaviour(target));
							inCombat = true;
						}
						return new AttackAction(target);
					}
				}
			}
		}
		for(Behaviour behaviour : behaviours.values()) {
			Action action = behaviour.getAction(this, map);
			if (action != null) {
				return action;
			}
		}
		return new DoNothingAction();
	}

	@Override
	/**
	 * Intrinsic weapon for goomba.
	 */
	protected IntrinsicWeapon getIntrinsicWeapon() {
		return new IntrinsicWeapon(10, "kicks");
	}

	@Override
	/**
	 * Kill goomba on reset.
	 */
    public void resetInstance(GameMap map) {
        map.removeActor(this);
    }
}
