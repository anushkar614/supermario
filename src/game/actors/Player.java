package game.actors;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.displays.Display;
import edu.monash.fit2099.engine.positions.Exit;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.displays.Menu;

import edu.monash.fit2099.engine.positions.Location;
import edu.monash.fit2099.engine.weapons.IntrinsicWeapon;
import game.behaviour.DrinkCapable;
import game.behaviour.Status;
import game.behaviour.ResetAction;
import game.behaviour.Resettable;
import game.grounds.Dirt;
import game.grounds.Fountain;
import game.items.Coin;
import game.Utilities;

import static game.behaviour.Status.ON_FOUNTAIN;

/**
 * Class representing the Player.
 */
public class Player extends ComponentActor implements Resettable, DrinkCapable {

	private final Menu menu = new Menu();
	private boolean canReset = true;
	private int baseDamage = 5;
	private int bonusIntrinsicAttackDamage = 0;

	/**
	 * Constructor of the player
	 *
	 * @param name        Name to call the player in the UI
	 * @param displayChar Character to represent the player in the UI
	 * @param hitPoints   Player's starting number of hitpoints
	 */
	public Player(String name, char displayChar, int hitPoints) {
		super(name, displayChar, hitPoints);
		// Register instance for resetting.
		registerInstance();
		this.addCapability(Status.HOSTILE_TO_ENEMY);
		addComponent(new WalletComponent(this));
		addComponent(new JumpComponent(this));
		addComponent(new BottleComponent(this));
	}

	/**
	 * Figures out what to do next.
	 * @see Status#JUMPABLE
	 * @see Status#POWERED
	 * @see ComponentId#TIMER_COMPONENT
	 * @see Status#SUPPLY_WATER
	 */
	@Override
	public Action playTurn(ActionList actions, Action lastAction, GameMap map, Display display) {
		Utilities.getInstance().tick();
		Component timer = getComponent(ComponentId.TIMER_COMPONENT);

		if (timer != null) {
			((TimerComponent)timer).updateTurns(this);
		}

		if (map.locationOf(this).getGround().hasCapability(Status.JUMPABLE) && hasCapability(Status.POWERED)) {
			map.locationOf(this).setGround(new Dirt());
			map.locationOf(this).addItem(new Coin(5, map.locationOf(this)));
		}

		// Status.ON_FOUNTAIN added to Actor capabilities if Player current ground is Fountain
		// Or if any Exit Grounds are Fountains

		// To fix this
		// If Player not currently standing on Fountain
		// but Exits around Player contain Fountain
		// then remove ON_FOUNTAIN status
		if (!map.locationOf(this).getGround().hasCapability(Status.SUPPLY_WATER)) {
			Location here = map.locationOf(this);

			for (Exit exit : here.getExits()) {
				Location destination = exit.getDestination();

				if (destination.getGround() instanceof Fountain) {
					this.removeCapability(ON_FOUNTAIN);
				}
			}
		}
			
		// Handle multi-turn Actions
		if (lastAction.getNextAction() != null)
			return lastAction.getNextAction();

		// Add the Reset Action if it hasn't been used.
		if (canReset) {
			actions.add(new ResetAction());
		}

		// Return/Print the Console Menu
		return menu.showMenu(this, actions, display);
	}

	@Override
	public char getDisplayChar(){
		return this.hasCapability(Status.TALL) ? Character.toUpperCase(super.getDisplayChar()): super.getDisplayChar();
	}

	/**
	 * Reset the player hp and status on reset.
	 * @param map the map where this actor resides.
	 */
	@Override
    public void resetInstance(GameMap map) {
    	// Heal Player to Full Health
    	heal(getMaxHp());
    	// TODO Reset player status.
    	canReset = false;
    }

	/**
	 * Creates and returns an intrinsic weapon.
	 *
	 * By default, the Actor 'punches' for 5 damage.
	 * In Player, this method is overridden to add bonus Intrinsic Attack Damage
	 * that has been accummulated from consuming Power Water
	 *
	 * @return a freshly-instantiated IntrinsicWeapon
	 */
	@Override
	protected IntrinsicWeapon getIntrinsicWeapon() {
		return new IntrinsicWeapon(5+bonusIntrinsicAttackDamage, "punches");
	}

	/**
	 * Adds bonus points to intrinsic attack damage from consuming water from Power Fountain
	 * @param points Additional intrinsic damage points to be added to accummulated bonus
	 */
	public void addBonusAttackDamage(int points) {
		bonusIntrinsicAttackDamage += points;
	}

}
