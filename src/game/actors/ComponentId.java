package game.actors;

/**
 * Set of all registered component ids.
 */
public enum ComponentId {
	WALLET_COMPONENT, //For the wallet
	JUMP_COMPONENT, //For the jump
	TIMER_COMPONENT, //For the timer
	BOTTLE_COMPONENT, //For the bottle
}