package game.actors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.displays.Display;
import edu.monash.fit2099.engine.actions.DoNothingAction;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.weapons.IntrinsicWeapon;
import edu.monash.fit2099.engine.positions.Location;

import game.behaviour.FollowBehaviour;
import game.behaviour.AttackAction;
import game.behaviour.Behaviour;
import game.behaviour.Status;
import game.behaviour.WanderBehaviour;
import game.behaviour.BreakAction;
import game.behaviour.Resettable;
import game.behaviour.Chattable;

/**
 * Public class Koopa enemy will attack mario if he gets too close and follow him after entering combat.
 * If defeated Koopa will retreat into his shell until smashed by mario with a wrench.
 */
public class Koopa extends ComponentActor implements Resettable, Chattable {
	// Holds messages used for chatting.
	protected final ArrayList<String> messages = new ArrayList<>();
	private final Map<Integer, Behaviour> behaviours = new HashMap<>(); // priority, behaviour
	private boolean inCombat = false;

	/**
	 * Allergic to floor prevents the enemy from entering Floor ground.
	 * Become dormant allows for the actor to avoid death by hiding.
	 * @see game.behaviour.Status#ALLERGIC_TO_FLOOR
	 * @see game.behaviour.Status#BECOME_DORMANT
	 */
	public Koopa(String flying_koopa, char f, int i) {
		super("Koopa", 'K', 100);
		// Add Messages used for chatting.
		messages.add("Never gonna make you cry!");
		messages.add("Koopi koopi koopii~!");
		// Register instance for resetting.
		registerInstance();
		this.behaviours.put(10, new WanderBehaviour());
		addCapability(Status.ALLERGIC_TO_FLOOR);
		addCapability(Status.BECOME_DORMANT);
	}

	/**
	 * At the moment, we only make it can be attacked by Player.
	 * @param otherActor the Actor that might perform an action.
	 * @param direction  String representing the direction of the other Actor
	 * @param map        current GameMap
	 * @return list of actions
	 * @see game.behaviour.Status#HOSTILE_TO_ENEMY
	 */
	@Override
	public ActionList allowableActions(Actor otherActor, String direction, GameMap map) {
		ActionList actions = new ActionList();
		if (!hasCapability(Status.DORMANT)) {
			if (otherActor.hasCapability(Status.HOSTILE_TO_ENEMY)) {
				actions.add(new AttackAction(this, direction));
			}
		}
		else {
			if (otherActor.hasCapability(Status.BREAKER)) {
				actions.add(new BreakAction(this, direction));
			}
		}
		return actions;
	}

	/**
	 * Figure out what to do next.
	 * @param actions
	 * @param lastAction
	 * @param map
	 * @param display
	 * @see Actor#playTurn(ActionList, Action, GameMap, Display)
	 * @return the appropriate actions
	 */
	@Override
	public Action playTurn(ActionList actions, Action lastAction, GameMap map, Display display) {
		// Perform chatting, messages must be in string array for chat.
		String[] messagesArray = new String[messages.size()];
		chat(this, display, messages.toArray(messagesArray));
		// Don't act if dormant. Could be a behaviour but wouldn't be worth 
		// the extra class for a single line, if more enemies did I'd consider.
		if (!hasCapability(Status.DORMANT)) {
			// This definitely wasn't the solution you were looking for but 
			// works pretty well and doesn't break the engine in any way. 
			// Couldn't figure out how to use the actionlist for this.
			// Checks each surrounding ground for an enemy, if one is found
			// then swap behaviour to follow and attack.
			Location location = map.locationOf(this);
			for(int xOff = -1; xOff <= 1; xOff++) {
				for(int yOff = -1; yOff <= 1; yOff++) {
					int locationX = location.x() + xOff;
					int locationY = location.y() + yOff;
					// Skip out of bounds locations.
					if (locationX < 0 || locationX > location.map().getXRange().max() 
						|| locationY < 0 || locationY > location.map().getYRange().max()) {
						continue;
					}
					Actor target = map.getActorAt(location.map().at(locationX, locationY));
					if (target != null) {
						if (target.hasCapability(Status.HOSTILE_TO_ENEMY)) {
							if (!inCombat) {
								behaviours.put(5, new FollowBehaviour(target));
								inCombat = true;
							}
							return new AttackAction(target);
						}
					}
				}
			}
			for(Behaviour Behaviour : behaviours.values()) {
				Action action = Behaviour.getAction(this, map);
				if (action != null)
					return action;
			}
		} 
		else {
			setDisplayChar('D');
		}
		return new DoNothingAction();
	}

	/**
	 * Intrinsic weapon for koopa.
	 * @return the new Intrinsic Weapon
	 */
	@Override
	protected IntrinsicWeapon getIntrinsicWeapon() {
		return new IntrinsicWeapon(30, "punches");
	}

	/**
	 * Kill koopa on reset.
	 * @param map the map where this actor resides.
	 */
	@Override
    public void resetInstance(GameMap map) {
        map.removeActor(this);
    }

}