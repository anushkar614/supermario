package game.actors;

import edu.monash.fit2099.engine.actors.Actor;
import game.behaviour.Status;

/**
 * Support more advanced jumping functionality in the future and ensures all 
 * jumping actors have the CAN_JUMP capability.
 * @see Status#CAN_JUMP
 */
public class JumpComponent extends Component {
    public JumpComponent(Actor actor) {
        super(ComponentId.JUMP_COMPONENT);
        actor.addCapability(Status.CAN_JUMP);
    }
}