package game.actors;

import edu.monash.fit2099.engine.actions.DoNothingAction;
import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.positions.GameMap;
import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.displays.Display;
import edu.monash.fit2099.engine.actors.Actor;

import game.items.PowerStar;
import game.items.WrenchItem;
import game.items.SuperMushroom;
import game.behaviour.Status;
import game.behaviour.PurchaseItemAction;
import game.behaviour.Chattable;

/**
 * A public class which represents Toad is a friendly npc who offers trading for mario in exchange for coins.
 */
public class Toad extends ComponentActor implements Chattable {
	// Potential Chat Messages
	static final String[] MESSAGES = {"You might need a wrench to smash Koopa's hard shells.",
		"You better get back to finding the Power Stars.",
		"The Princess is depending on you! You are our only hope.",
		"Being imprisoned in these walls can drive a fungus crazy :("};

	/**
	 * Public constructor for Toad
	 */
	public Toad() {
		super("Toad", 'O', 99999);
	}

	/**
	 * The playTurn function doesn't have any actions other than providing trading.
	 * @param actions
	 * @param lastAction
	 * @param map
	 * @param display
	 * @return invokes the nothing action
	 * @see Actor#playTurn(ActionList, Action, GameMap, Display)
	 */
	@Override
	public Action playTurn(ActionList actions, Action lastAction, GameMap map, Display display) {
		// Perform chatting.
		chat(this, display, MESSAGES);
		return new DoNothingAction();
	}

	/**
	 * Offers trading to nearby actors who have a wallet.
	 * Can sell PowerStars, SuperMushrooms and Wrenches.
	 * @param otherActor
	 * @param direction
	 * @param map
	 * @see game.behaviour.Status#HAS_WALLET
	 * @return actions list for allowable actions.
	 */
	@Override
	public ActionList allowableActions(Actor otherActor, String direction, GameMap map) {
		ActionList actions = new ActionList();
		if (otherActor.hasCapability(Status.HAS_WALLET)) {
			actions.add(new PurchaseItemAction(new PowerStar(), 600));
			actions.add(new PurchaseItemAction(new SuperMushroom(), 400));
			actions.add(new PurchaseItemAction(new WrenchItem(), 200));
		}
		return actions;
	}
}