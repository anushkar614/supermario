package game.actors;

import edu.monash.fit2099.engine.actions.Action;
import edu.monash.fit2099.engine.actions.ActionList;
import edu.monash.fit2099.engine.actions.DoNothingAction;
import edu.monash.fit2099.engine.actors.Actor;
import edu.monash.fit2099.engine.displays.Display;
import edu.monash.fit2099.engine.positions.GameMap;
import game.InstanceChecker;
import game.behaviour.RescueAction;
import game.behaviour.Status;
import game.behaviour.Chattable;

/**
 * Class representing Princess Peach
 */
public class PrincessPeach extends Actor implements Chattable {
    static final String[] MESSAGES = {"Dear Mario, I'll be waiting for you...",
                                      "Never gonna give you up!",
                                      "Release me, or I will kick you!"};

    /**
     * Constructor of Princess Peach
     */
    public PrincessPeach() {
        super("Princess Peach", 'P', 100);
    }

    /**
     * Figures out what to do next.
     * @param actions    collection of possible Actions for this Actor
     * @param lastAction The Action this Actor took last turn. Can do interesting things in conjunction with Action.getNextAction()
     * @param map        the map containing the Actor
     * @param display    the I/O object to which messages may be written
     * @return do no action
     */
    @Override
    public Action playTurn(ActionList actions, Action lastAction, GameMap map, Display display) {
        // Perform Chatting
        chat(this, display, MESSAGES);
        return new DoNothingAction();
    }

    /**
     * Returns collection of the Actions that the otherActor could do to the current Actor.
     * It can be attacked by a player
     * @param otherActor the otherActor that could perform an action.
     * @param direction  type String, tells the direction of the otherActor
     * @param map        GameMap current
     * @return list of new collection of actions that otherActor will be able to do to the current Actor
     */
    @Override
    public ActionList allowableActions(Actor otherActor, String direction, GameMap map) {
        ActionList actions = new ActionList();
        if (InstanceChecker.getInstance().getPlayers().contains(otherActor)) {
            actions.add((ActionList) new RescueAction(direction, this));
        }
        return actions;
    }
}