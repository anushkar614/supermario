package game.actors;

import edu.monash.fit2099.engine.actors.Actor;

import game.behaviour.Status;

public class TimerComponent extends Component {
    private Status statusToRemove;
    private int turnsLeft = 10;

    public TimerComponent(Status statusToRemove, int turns) {
        super(ComponentId.TIMER_COMPONENT);
        turnsLeft = turns;
    }

    public void updateTurns(Actor actor) { 
        turnsLeft -= 1;
        if (turnsLeft < 0) {
            if (actor.hasCapability(Status.POWERED)) {
                actor.removeCapability(Status.POWERED);
            }
        }
    }
}
