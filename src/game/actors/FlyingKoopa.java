package game.actors;

import game.behaviour.Status;

/**
 * Class representing Flying Koopa
 */

public class FlyingKoopa extends Koopa {
    
    /**
     * Constructor of the FlyingKoopa
     */
    public FlyingKoopa() {
        super("Flying Koopa", 'F', 150);
        this.addCapability(Status.FLYING);                  // able to fly highgrounds
    }

}
