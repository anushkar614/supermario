# Super Mario-Inspired Game

## General Background
Welcome to the world of text-based "rogue-like" gaming! Rogue-like games draw inspiration from the original fantasy game named Rogue, which became popular in the early days of home computing. These games, characterized by their ASCII graphics and deep gameplay mechanics, have maintained a dedicated following despite the evolution of gaming technology.

https://drive.google.com/file/d/1iKgPukII1uH1tlPWot0R38aEqMvlEuns/view?usp=sharing

## Overview
Embark on an adventure in the magical land of Mushroom Kingdom, where you will play as Mario to explore and conquer various challenges inspired by the iconic Super Mario game. This text-based rogue-like game brings the beloved characters, items, and locations of the Super Mario universe to life in a unique and immersive experience.

## Game Features

## 1: Let it grow! 🌳
In World 1-1, you'll encounter Trees that go through three stages: Sprout (+), Sapling (t), and Mature (T). Each stage has unique spawning abilities and characteristics.  It takes 10 turns to grow into the next stage. 
- Sprout (+) It has a 10% chance to spawn Goomba on its position in every turn. If any actor stands on it, it cannot spawn Goomba. It takes 10 turns to grow into a small tree/Sapling (t)
- Sapling (t): It has a 10% chance to produce/spawn a coin ($20) on its location at every turn. It takes another 10 turns to grow into a tall tree/Mature(T)
- Mature (T): It has a 15% chance to spawn Koopa at every turn. If an actor stands on it, it cannot spawn Koopa. For every 5 turns, It can grow a new sprout (+) in one of the surrounding fertile squares, randomly. If there is no available fertile square, it will stop growing sprouts. At the moment, the only fertile ground is Dirt. It has 20% to wither and die (becomes Dirt) at every turn.

![image](https://github.com/AnushkaReddy-hub/Supermario/assets/77491598/3192bc87-437d-40e5-9357-2cb1332fa6ed)

Legend: $ coin, +sprout, t sapling, T tree,  g Goomba, K Koopa, m Mario, O Toad, #wall, dirt, _ floor, ^ super mushroom.

### 2: Jump Up, Super Star! 🌟
When the actor is standing next to the high ground, the actor can jump onto it. Going up requires a jump, but going down doesn't require it (the actor can walk normally). Each jump has a certain success rate. If the jump is unsuccessful, the actor will fall and get hurt.  The success rate and fall damage vary based on the destination ground type as listed below:
- Wall: 80% success rate, 20 fall damage
- Tree:
  - Sprout(+): 90% success rate, 10 fall damage
  - Sapling(t): 80% success rate, 20 fall damage
  - Mature(T): 70% success rate,  30 fall damage
    
For instance, Mario tries to jump from Dirt to Wall. Unfortunately, the jump fails, Mario stays on the current ground (e.g., Dirt), and he receives 20 damage (from the Wall). The next attempt is a success and Mario is standing on the Wall now. If Mario tries to move to a Tree next to his current position, he must jump again. Otherwise, he can walk down to Dirt, Floor, and other grounds freely. 
When Mario consumes Super Mushroom 🍄 (see Magical Items), he can jump freely with a 100% success rate and no fall damage.

How about the enemy? ☠️ The enemy cannot jump at all, but it can walk down to the lower ground normally (due to wandering or following). For instance, the enemy standing on the Tree can walk down to the Dirt freely. But, it cannot jump to other high grounds (any kind of Trees and Wall). 

### 3: Enemies ☠️
There are two enemies at the current stage: Goomba and Koopa Troopa. Once the enemy is engaged in a fight (the Player attacks the enemy or the enemy attacks you -- when the player stands in the enemy's surroundings), it will follow the Player. The unconscious enemy must be removed from the map. All enemies cannot enter the Floor (_).
1. Goomba (g) 🍄 
Goombas /ˈɡuːmbə/, known in Japan as Kuribo, are a fictional mushroom-like species from Nintendo's Mario franchise. Goomba is a basic enemy. In Japan, Goombas are called "Kuribō", which loosely translates as "chestnut person".[7]
- It starts with 20 HP
- It attacks with a kick that deals 10 damage with a 50% hit rate.
- At every turn, it has a 10% chance of being removed from the map (suicide). The main purpose is to clean up the map.
2. Koopa (K)🐢 
Koopa Troopas, commonly shortened to Koopas or Troopas, known in Japan as Nokonoko, are reptilian mini-troopers of the Koopa Troop from the Mario franchise. When defeated, Koopas may flee from or retreat inside their shells. 
- It starts with 100 HP
- When defeated, it will not be removed from the map. Instead, it will go to a dormant state (D) and stay on the ground (cannot attack nor move).
- Mario needs a Wrench (80% hit rate and 50 damage), the only weapon to destroy Koopa's shell.
- Destroying its shell will drop a Super Mushroom.

### 4: Magical Items 🍄
1. Super Mushroom 🍄 
Anyone who consumes Super Mushroom ^ will experience the following features:
- increase max HP by 50
- the display character evolves to the uppercase letter (e.g., from m to M).
- it can jump freely with a 100% success rate and no fall damage.

The effect will last until it receives any damage (e.g., hit by the enemy). Once the effect wears off, the display character returns to normal (lowercase), but the maximum HP stays. 

2. Power Star 🌟 
Power Star * cannot stay in the game forever. It will fade away and be removed from the game within 10 turns (regardless it is on the ground or in the actor's inventory). Anyone who consumes a Power Star will be healed by 200 hit points and will become invincible. The invincible effect replaces fading duration (aka, fading turn's ticker stops). Here, the invincible effect lasts for 10 turns.
- Higher Grounds. The actor does not need to jump to higher level ground (can walk normally). If the actor steps on high ground, it will automatically destroy (convert) the ground to Dirt.
- Convert to coins. For every destroyed ground, it drops a Coin ($5).
- Immunity. All enemy attacks become useless (0 damage).
- Attacking enemies. When active, a successful attack will instantly kill enemies.
  
### 5: Trading 💰
The coin($) is the physical currency that an actor/buyer collects. A coin has an integer value (e.g., $5, $10, $20, or even $9001). Coins will spawn randomly from the Sapling (t) or from destroyed high grounds. Collecting these coins will increase the buyer's wallet (credit/money). Using this money, buyers (i.e., a player) can buy the following items from Toad:

1. Wrench: $200
2. Super Mushroom: $400
3. Power Star: $600

Toad (O) is a friendly actor, and he stands in the middle of the map (surrounded by brick Walls). 

### 7: Reset Game⏱️
Sometimes, the game can become overwhelming and it is hard to walk around (too many Koopas!). Mario has an action to reset the game at any time. If it is executed, it will do the following:
- Trees have a 50% chance to be converted back to Dirt
- All enemies are killed.
- Reset player status (e.g., from Super Mushroom and Power Star)
- Heal player to maximum
- Remove all coins on the ground (Super Mushrooms and Power Stars may stay).
Mario can only do this once throughout the entire game.

### 31: A challenging new map - Lava zone 🔥
1. New Map 🗺️
Contains some random blazing fire grounds (Lava L) that will inflict 15 damage per turn when the player steps on them. Enemies cannot step on this lava.
2. Teleportation (Warp Pipe) c
Mario can teleport to the other map back and forth through a warp pipe C. However, the warp pipe is blocked by a Piranha Plant. Once you kill Piranha Plant, you can stand on the pipe (i.e., using jump) and use it to teleport to another pipe on the second map. You can travel back to the last/previous pipe that teleported you before.

### 32: More allies and enemies! 🦸☠️
1. Princess Peach P 👸🍑 
Princess Peach is held captive by Bowser! She is on the Dirt in the Lava zone, and she stands next to Bowser and cannot move, attack, or be attacked. Once you have defeated Bowser and obtained a key, you can interact with her to end the game with a victory message!
2. Bowser B 🐢😈 
Bowser will stand still, waiting for Mario on the second map. Once Mario stands next to him, Bowser will attack and follow Mario! He will attack whenever possible. Whenever Bowser attacks, a fire will be dropped on the ground that lasts for three turns. That fire will deal 20 damage per turn to anyone. When the Bowser is killed, it will drop a key to unlock Princess Peach's handcuffs.
It has the following stats:
- 500 hit points
- "punch" attack with a 50% hit rate and 80 damage.
Resetting the game (r command) will move Bowser back to the original position, heal it to the maximum, and it will stand there until Mario is within Bowser's attack range.
3. Piranha Plant Y 🐟🥀 
Piranha Plant Y will spawn at the second turn of the game (note: the first turn is when you start the game for the first time). This plant will attack Mario when he stands next to it. Piranha Plant cannot move around. It will not follow anyone when it is engaged in a fight. It has the following stats:
- 150 hit points
- "chomps" attack with 50% hit rate and 90 damage.
Once the player kills it, the corresponding WarpPipe will not spawn Piranha Plant again until the player resets the game (r command). Resetting will increase alive/existing Piranha Plants' hit points by an additional 50 hit points and heal to the maximum.
4. Flying Koopa F 💸🐢
Flying Koopa F has pretty much similar characteristics as the original Koopa, except it has a bigger health bar (150 hit points). Furthermore, it can walk (fly) over the trees and walls when it wanders around (incl. other high grounds). It also applies when Flying Koopa follows Mario. Like Koopa, it will go to a dormant state whenever it is unconscious until Mario brings a wrench to crack the shell (killing it)! Cracking its shell drops a Super Mushroom. The mature tree has a 50:50 chance of spawning either a normal Koopa or a Flying Koopa (after a successful 15% spawn rate).

### 33: Magical fountain ⛲
1. Bottle b 🥛 
A magical bottle that contains unlimited magical waters. Mario will have this bottle in its inventory at the beginning of the game (before it starts). This bottle will be a permanent item that cannot be dropped or picked up. This bottle can be filled with water, and Mario can drink/consume the water inside the bottle. Each water will give a unique effect depending on its source (see Fountains below).
2. Fountains ⛲ 
Mario can refill the bottle when he stands on the fountain. A fountain produces an endless amount of water. Each water from a fountain provides different effects that will help Mario in his journey.
2a. Health Fountain H 🩸 Drinking this water will increase the drinker's hit points/healing by 50 hit points.
2b. Power Fountain A 💪 When the water is consumed, it increases the drinker's base/intrinsic attack damage by 15.

Note: When attacking an enemy while having a weapon in the inventory (equipping), the hit rate and attack damage will be based on that weapon. For example, Mario has 200 base attack damage, but it holds a Wrench that has an 80% hit rate and 50 damage. When Mario attacks Goomba, the damage will be 50 points.

### 34: Flowers 🌻 (Structured Mode)
1. Fire Flower f 🌻 
For every growing stage of the tree (Sprout -> Sapling and Sapling -> Mature), it has a 50% chance to spawn (or grow) a Fire Flower on its location. Mario can consume this fire flower to use Fire Attack. There could be multiple Fire Flowers in one location.
2. Fire attack v 🔥 
Once the actor consumes the Fire Flower, it can use fire attack action on the enemy. Attacking will drop a fire v at the target's ground. This "fire attack" effect will last for 20 turns (i.e., it is pretty similar to being INVINCIBLE for 10 turns). The fire will stay on the ground for three turns. The fire will deal 20 damage per turn. 

### 35: Speaking 🗣️(Structured Mode)
Listen to characters' monologues at alternate turns, adding depth and immersion to the game world.
https://drive.google.com/file/d/1wOpsZKgA5BcaAS5SjmmYZOcuni1wOfdx/view?usp=sharing

## Installation

1. Clone the game repository to your local machine.
2. Open a command prompt or terminal in the directory where you cloned the repository.
3. Run the following commands to compile the Java code:
   
    ```bash
    compile.bat
    ```
    
This script recursively searches for all Java files (`*.java`) in the current directory and its subdirectories, compiles them using `javac`, and sets a dummy variable `DUMMY` with the message "I refuse to use an IDE."

## Credits
This game is inspired by Nintendo's Super Mario franchise. All references to characters, items, and concepts belong to their respective creators and are used for educational and entertainment purposes only.
